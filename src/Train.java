/*
 * Concurrent Programming - Assignment 2
@author Mohamed El Gueloui <mohamed.elgueloui@edu.hefr.ch>

 @version 1.0

 @since 15-05-2012


 Honor Code : I pledge that this program represents my own

 program code . I received help from no one in designing

 and debugging my program but i search a lot in the internet.
*/

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * 
 * @author 
 *
 */
public class Train extends Thread {
	public enum	STATES {
		READY,
		RUNNING,
	};
	
	/*
	 * Time used to check the Train before the NextRide
	 */
	public static final int Maintenace_TIME = 1000;
	public static final int RIDE_TIME = 1000;
	
	public static final int ROLLER_PLACES = 6;
	public static final int TOT_CLIENTS = 20;
	
	/*
	 * state of the train
	 */
	public STATES state = STATES.READY;
	
	// waiting queue for the train to comme 
	private LinkedList<Client> wqueue = new LinkedList<Client>();
	// places in the train
	private ArrayList<Client> ptrain = new ArrayList<Client>();
	
	public Train() {
		
	}
	
	synchronized public void ride(Client client)
	{
		if (state == STATES.READY) {
			if(ptrain.size() < ROLLER_PLACES) {
				ptrain.add(client);
				client.get_in();
			}
			else {
				wqueue.add(client);
				client.in_queue();
			}
		}
		else {
			wqueue.add(client);
			client.in_queue();
		}
			
	}
	/**
	 * make the train ready, all passenger out
	 */
	private void ready() {
		for (Client client : ptrain) {
			client.get_off();
		}
		ptrain.clear();
		System.out.println("Train Ready for the next ride");
		state = STATES.READY;
		
		for (int i = 0; i < TOT_CLIENTS && !wqueue.isEmpty(); i++) {
			Client c = wqueue.pop();
			ptrain.add(c);
			c.get_in();
		}
	}
	
	public void run() {
		try {
			Thread.sleep(4000);
			while (true) {
				// Accepting clients
				System.out.println("Train Ready");
				// Train going on ride
				
				System.out.println("train taking off");
				state = STATES.RUNNING;
				for(Client c: ptrain) {
					c.emotion();
				}
				Thread.sleep(RIDE_TIME);
				// End of the ride
				System.out.println("train arrive");
				ready();
				// prepare train for the next ride
				Thread.sleep(Maintenace_TIME);
			}
		} catch (InterruptedException e) {
			System.err.println("Exception in Train :\n" + e.getMessage());
		}
	}

}
