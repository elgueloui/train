/*
 * Concurrent Programming - Assignment 2
@author Mohamed El Gueloui <mohamed.elgueloui@edu.hefr.ch>

 @version 1.0

 @since 15-05-2012


 Honor Code : I pledge that this program represents my own

 program code . I received help from no one in designing

 and debugging my program but i search a lot in the internet.
*/

public class Client extends Thread {
	public enum STATES {
		RIDING,
		WAITING,
		OFF,
		READY, //
	}
	public STATES state = STATES.READY;

	private String name;
	private Train train;

	public Client(String name, Train train) {
		this.name = name;
		this.train = train;
	}
	// get in the train
	public void get_in() 
	{
		System.out.println(name + " get in the train");
		state = STATES.RIDING;
	}
	// we need a FIFO in order that customer can wait
	public void in_queue()
	{
		System.out.println(name + " waiting for train");
		state = STATES.WAITING;
	}
	//get off the train
	public void get_off() 
	{
		System.out.println(name + " get off the train");
	}
	
	public void emotion() 
	{
		System.out.println(name + " in emotion");
	}
	
	

	
	public void run() {
		try {
			// 1 to 10 sec to decide ton ride the roller
			Thread.sleep(10000);
			while (true) {
				System.out.println(name + " Wants to ride");
				train.ride(this);
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
            System.err.println("Exception in Client.java:\n" + e.getMessage());
        }
	}
}