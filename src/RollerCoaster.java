/*
 * Concurrent Programming - Assignment 2
@author Mohamed El Gueloui <mohamed.elgueloui@edu.hefr.ch>

 @version 1.0

 @since 15-05-2012


 Honor Code : I pledge that this program represents my own

 program code . I received help from no one in designing

 and debugging my program but i search a lot in the internet.
*/

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 *
 * Client of the roller can be  in the following state
 * 	 - ready : want to get aboard
 *   - shaking: from the riding experience
 *   - waiting : waiting the roller to come
 *   - riding : on the Roller
 *   
 *  Roller States :
 *   - Running
 *   - Ready : to take Clients
 *   - Preparing : prepare 
 *   
 *   When the ride is finished notify client to get off the Roller
 */
public class RollerCoaster {
	static final int ROLLER_PLACES = 6;
	static final int TOT_CLIENTS = 20;

	public  ArrayList<Client> clients = new ArrayList<Client>();
	public Train train = new Train(); 
	
	
	public RollerCoaster() {
		
		for(int i= 0; i < TOT_CLIENTS; i++)
		{
			clients.add(new Client("NAME"+i, train));
		}
	}
	
// roller ready <=> accepting clients

	public static void main(String[] args) {
		RollerCoaster roller = new RollerCoaster();
		
		ExecutorService threadExecutor = Executors.newCachedThreadPool();
		
		threadExecutor.execute(roller.train);
		
		for(Client c: roller.clients) 
		{
			threadExecutor.execute(c);
		}
		
	}
}
